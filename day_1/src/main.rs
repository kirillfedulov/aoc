use std::fs;

fn main() {
    let mut floor = 0;
    let input = fs::read("input.txt").unwrap();
    for (i, c) in input.iter().enumerate() {
        if *c == b'(' {
            floor += 1;
        } else if *c == b')' {
            floor -= 1;
        }
        if floor == -1 {
            println!("{}", i + 1);
            break;
        }
    }
    //println!("{}", floor);
}
