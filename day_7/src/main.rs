use std::fs;
use std::collections::HashMap;

enum Binary {
    And(Value, Value),
    Or(Value, Value),
    RightShift(Value, u16),
    LeftShift(Value, u16)
}

impl Binary {
    fn from_string(left: &str, op: &str, right: &str) -> Self {
        match op {
            "AND" => Binary::And(Value::from_string(left), Value::from_string(right)),
            "OR"  => Binary::Or(Value::from_string(left), Value::from_string(right)),
            "RSHIFT" => Binary::RightShift(Value::from_string(left), right.parse::<u16>().unwrap()),
            "LSHIFT" => Binary::LeftShift(Value::from_string(left), right.parse::<u16>().unwrap()),
            _        => unreachable!()
        }
    }
}

enum Unary {
    Not(Value)
}

impl Unary {
    fn from_string(left: &str, op: &str) -> Self {
        match op {
            "NOT" => Unary::Not(Value::from_string(left)),
            _     => unreachable!()
        }
    }
}

enum Value {
    Constant(u16),

    Identifier(String)
}

impl Value {
    fn from_string(value: &str) -> Self {
        match value.parse::<u16>() {
            Ok(constant) => Value::Constant(constant),
            _            => Value::Identifier(value.to_string())
        }
    }
}

enum Gate {
    Value(Value),

    Unary(Unary),

    Binary(Binary)
}

impl Gate {
    fn value(value: &str) -> Self {
        Gate::Value(Value::from_string(value))
    }

    fn unary(left: &str, op: &str) -> Self {
        Gate::Unary(Unary::from_string(left, op))
    }

    fn binary(left: &str, op: &str, right: &str) -> Self {
        Gate::Binary(Binary::from_string(left, op, right))
    }
}


fn optimize_value(value: &mut Value, gates: &mut HashMap<String, Gate>) {
//    match value {
//        Value::Constant(_) => (),
//        Value::Identifier(id) => optimize_gate(id, &mut gates[id], gates)
//    }
}

fn optimize_unary(id: &str, unary: &mut Unary, gates: &mut HashMap<String, Gate>) {
//    match unary {
//        Unary::Not(value) => {
//            optimize_value(&mut value, gates);
//            
//            //match gates[id] {
//                //Gate::Unary(Unary::Not(Value::Constant(constant))) => 
//        }
//    }
}

fn optimize_binary(id: &str, binary: &mut Binary, gates: &mut HashMap<String, Gate>) {
}


fn optimize_gate(id: &str, gates: &mut HashMap<String, Gate>) {
    let gate = gates[id];

    match gate {
        Gate::Value(value) => optimize_value(&mut value, gates),
        Gate::Unary(unary) => optimize_unary(id, &mut unary, gates),
        Gate::Binary(binary) => optimize_binary(id, &mut binary, gates)
    }
}

fn optimize(gates: &mut HashMap<String, Gate>) {
    let ids: Vec<String>= gates.keys().cloned().collect();

    for id in ids {
        optimize_gate(&id, gates);
    }
}

fn main() {
    let input = fs::read_to_string("input.txt").expect("Failed to read input file");

    let mut gates = HashMap::new();

    for line in input.lines() {
        let mut args = line.split_whitespace();

        let first = args.next().unwrap();
        let second = args.next().unwrap();
        match second {
            "->" => {
                let id = args.next().unwrap();

                gates.insert(id.to_string(), Gate::value(first));
            },

            "AND" | "OR" | "RSHIFT" | "LSHIFT" => {
                let third = args.next().unwrap();

                args.next(); // skip '->'

                let id = args.next().unwrap();

                gates.insert(id.to_string(), Gate::binary(first, second, third));
            }

            _ => {
                assert!(first == "NOT");

                args.next(); // skip '->'

                let id = args.next().unwrap();

                gates.insert(id.to_string(), Gate::unary(second, first));
            }
        }
    }

    let mut i = 1;
    loop {
        println!("Optimization pass {i}");

        optimize(&mut gates);

        match gates["a"] {
            Gate::Value(Value::Constant(constant)) => {
                println!("{constant}");
                break;
            },

            _ => i += 1
        }
    }
}
