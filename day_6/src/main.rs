use std::fs;

fn main() {
    let mut grid = vec![0; 1_000_000];

    let input = fs::read_to_string("input.txt").expect("Failed to read input file");

    for line in input.lines() {
        let mut args = line.split_whitespace();

        let command = args.next().unwrap();
        let x1= args.next().unwrap().parse::<u32>().unwrap();
        let y1= args.next().unwrap().parse::<u32>().unwrap();
        let x2= args.next().unwrap().parse::<u32>().unwrap();
        let y2= args.next().unwrap().parse::<u32>().unwrap();
        
        for x in x1..x2+1 {
            for y in y1..y2+1 {
                let i= (x*1000+y) as usize;

                match command {
                    "turnon"  => grid[i] += 1,
                    "turnoff" =>
                        if grid[i] != 0 {
                            grid[i] -= 1;
                        }
                    ,
                    "toggle"  => grid[i] += 2,
                    _         => ()
                }
            }
        }
    }

    let lit_count = grid.iter().fold(0, |a, x| a + x);
    println!("{lit_count}");
}
