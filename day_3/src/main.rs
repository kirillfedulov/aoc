use std::fs;
use std::collections::HashSet;

#[derive(PartialEq, Eq, Hash, Clone)]
struct House(i32, i32);

impl House {
    fn move_by(&mut self, direction: char) {
        match direction {
            '<' => self.0 -= 1,
            '>' => self.0 += 1,
            '^' => self.1 += 1,
            'v' => self.1 -= 1,
            _   => ()
        }
    }
}

fn main() {
    let input = fs::read_to_string("input.txt").expect("Failed to read input file");

    let mut presents = 1;

    let mut visited= HashSet::new();

    let mut santa_house = House(0, 0);
    let mut robo_house = House(0, 0);

    visited.insert(santa_house.clone());

    for (i, direction) in input.chars().enumerate() {
        if i % 2 == 1 {
            santa_house.move_by(direction);

            if !visited.contains(&santa_house) {
                presents += 1;
                visited.insert(santa_house.clone());
            }
        } else {
            robo_house.move_by(direction);

            if !visited.contains(&robo_house) {
                presents += 1;
                visited.insert(robo_house.clone());
            }
        }
    }

    println!("{presents}");
}
