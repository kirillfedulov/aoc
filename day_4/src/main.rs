use std::ops::Add;

extern crate md5;

fn main() {
    let secret = String::from("ckczppom");
    let mut i = 1;

    loop {
        let input = secret.clone().add(&i.to_string());
        let digest = format!("{:x}", md5::compute(input));
        if digest.starts_with("000000") {
            break;
        }

        i += 1;
    }

    println!("{i}");
}
