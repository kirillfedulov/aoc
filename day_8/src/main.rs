use std::fs;

fn main() {
    let input = fs::read_to_string("input.txt").expect("Failed to read input file");

    let mut difference = 0;

    for line in input.lines() {
        let mut iter = line.chars();

        let mut escaped_len = 2;
        while let Some(ch) = iter.next() {
            match ch {
                '"' => escaped_len += 2,
                '\\' => escaped_len += 2 ,
                _ => escaped_len += 1
            }
        }

        difference += escaped_len - line.len();
    }

    println!("{difference}");
}
