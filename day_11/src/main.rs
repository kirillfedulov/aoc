fn main() {
    let mut input = String::from("cqjxjnds");
    let len = input.len();

    'outer: for i in (0..len).rev() {
        unsafe {
            let bytes = input.as_bytes_mut();

            loop {
                if bytes[i] >= b'z' {
                    bytes[i] = b'a';

                    break;
                }

                bytes[i] += 1;

                if bytes[i] == b'i' || bytes[i] == b'o' ||
                   bytes[i] == b'l' {
                       bytes[i] += 1;

                       continue;
                }

                if i+1==len {
                    if bytes[i-1] != bytes[i] ||
                      (bytes[i-1] != bytes[i]-1 && bytes[i-2] != bytes[i]-2) {
                       bytes[i] += 1;

                       continue;
                    }
                } else if i==0 {
                    if bytes[i+1] != bytes[i] ||
                      (bytes[i+1] != bytes[i]+1 && bytes[i+2] != bytes[i]+2) {
                       bytes[i] += 1;

                       continue;
                    }
                } else {
                    if bytes[i+1] != bytes[i] || bytes[i-1] != bytes[i] ||
                      (bytes[i-1] != bytes[i]-1 && bytes[i+1] != bytes[i]+1) {
                       bytes[i] += 1;

                       continue;
                    }
                }

                break 'outer;
            }
        }
    }

    println!("{input}");
}
