#!/bin/sh

grep -E ".*[aeiou].*[aeiou].*[aeiou].*" input.txt | grep -E "(.)\1" | grep -Ev "ab" | grep -v "cd" | grep -v "pq" | grep -v "xy" | wc -l
