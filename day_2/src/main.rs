use std::fs::File;
use std::io::Read;
use std::cmp;

fn main() {
    let mut feet = 0;
    let mut infile = File::open("input.txt").expect("Failed to open input file");
    let mut input = String::new();
    infile.read_to_string(&mut input).expect("Failed to read input file");
    for line in input.lines() {
        let mut iter = line.split_whitespace();
        let l = iter.next().unwrap().parse::<u32>().unwrap();
        let w = iter.next().unwrap().parse::<u32>().unwrap();
        let h = iter.next().unwrap().parse::<u32>().unwrap();
        feet += 2*l*w + 2*w*h + 2*l*h + cmp::min(l*w, cmp::min(w*h, l*h));
    }
    println!("{feet}");
}
