use std::ops::AddAssign;

fn main() {
    let mut input = String::from("1113222113");
    let mut s     = String::new();

    for _ in 0..50 {
        let mut nch = 1;
        let mut chars = input.chars();
        let mut ch    = chars.next().unwrap();
        while let Some(it) = chars.next() {
            if it == ch {
                nch += 1;
            } else {
                s.add_assign(&nch.to_string());
                s.push(ch);

                ch  = it;
                nch = 1;
            }
        }
        s.add_assign(&nch.to_string());
        s.push(ch);

        println!("{} {}", s, s.len());

        input = s.clone();
        s.clear();
    }
}
